import axios from "axios";
import dotenv from "dotenv";

import { Feed, Order } from "./types";

// https://developers.vtex.com/docs/api-reference/orders-api#get-/api/orders/feed

dotenv.config();

axios
  .get<Feed[]>("https://coolshoppartnerit.myvtex.com/api/orders/feed", {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "X-VTEX-API-AppKey": process.env.APP_KEY,
      "X-VTEX-API-AppToken": process.env.APP_TOKEN,
    },
  })
  .then(({ data }) => {
    loadOrders(data);
  })
  .catch((error) => {
    console.error(error);
  });

async function loadOrders(feed: Feed[]): Promise<void> {
  if (feed.length === 0) {
    return;
  }

  const promises = feed.map(async (feed) => {
    const { orderId } = feed;

    const { data: order } = await axios.get<Order>(
      `https://coolshoppartnerit.myvtex.com/api/oms/pvt/orders/${orderId}`,
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "X-VTEX-API-AppKey": process.env.APP_KEY,
          "X-VTEX-API-AppToken": process.env.APP_TOKEN,
        },
      }
    );

    console.log(order);
  });

  await Promise.all(promises).catch((error) => {
    console.error(error);
  });
}
