export type Feed = {
  eventId: null;
  handle: string;
  domain: string;
  state: string;
  lastState: unknown;
  orderId: string;
  lastChange: string;
  currentChange: string;
  availableDate: string;
};

export type Order = {
  orderId: "1244730712239-01";
  sequence: "502790";
  marketplaceOrderId: "";
  marketplaceServicesEndpoint: null;
  sellerOrderId: "SLR-1244730712239-01";
  origin: "Marketplace";
  affiliateId: "";
  salesChannel: "1";
  merchantName: null;
  status: "payment-approved";
  statusDescription: "Pagamento Aprovado";
  value: 2012;
  creationDate: "2022-07-06T09:11:51.4597231+00:00";
  lastChange: "2022-07-06T09:12:00.2849749+00:00";
  orderGroup: "1244730712239";
};
