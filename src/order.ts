import axios from "axios";

// https://developers.vtex.com/docs/api-reference/checkout-api#put-/api/checkout/pub/orders

axios
  .put(
    "https://coolshoppartnerit.vtexcommercestable.com.br/api/checkout/pub/orders",
    {
      items: [
        {
          id: "3",
          quantity: 1,
          seller: "1",
          price: 10000,
        },
      ],
      clientProfileData: {
        email: "francesco.casetta@coolshop.it",
        firstName: "Testing",
        lastName: "VTEX",
        document: "078051120",
        documentType: "ssn",
        phone: "1234567890",
        corporateName: null,
        tradeName: null,
        corporateDocument: null,
        stateInscription: null,
        corporatePhone: null,
        isCorporate: false,
      },
      shippingData: {
        address: {
          addressType: "residential",
          postalCode: "12046",
          city: "Torino",
          state: "TO",
          country: "ITA",
          street: "Via dela Rocca",
          number: "5",
          neighborhood: "Botafogo",
          complement: "",
          reference: "",
          geoCoordinates: [],
        },
        logisticsInfo: [
          {
            itemIndex: 0,
            selectedSla: "Normal",
            price: 1094,
          },
        ],
      },
      paymentData: {
        payments: [
          {
            paymentSystem: "2",
            referenceValue: 11094,
            value: 11094,
            installments: 1,
          },
        ],
      },
    },
    {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-VTEX-API-AppKey": "vtexappkey-coolshoppartnerit-RISTBT",
        "X-VTEX-API-AppToken":
          "JGOGNMKNGVSZCBOYQKFLFGMCSTAALDCVRCWQPXSFTPZBWSJTNNUYEMWVZWLRSWIMHBKNWZYXILVAPOSENLCFRTYUHFYTNGHYOBDAAEDARVORDEQGZFQUVUFLFMZMOGWY",
      },
    }
  )
  .then((resp) => {
    console.log(resp);
  })
  .catch((error) => {
    console.error(error);
  });
